from flask import Flask, request, jsonify
from catboost import CatBoostClassifier, Pool
import requests

app = Flask(__name__)


@app.route('/train', methods=['GET'])
def predict_router():
    model_path = 'model/traveldiction.cbm'
    
    CATS = ['Pays', 'Gender', 'Status', 'Religion', 'native_language', 
            'i1', 'i2', 'i3']
    NUMERICS = ['Age']
    
    data = get_all_users()

    if data:       
        model = CatBoostClassifier()       
    
        pool = Pool(data=data,
                            cat_features=CATS, 
                            feature_names=CATS+NUMERICS)

        model.fit(pool, use_best_model=True, plot=True)      
        model.save_model('model/traveldiction', format="cbm", export_parameters=None,
           pool=None)
    else:
        return "bad request"

def get_all_users():
    columns = ['Pays', 'Gender', 'Status', 'Race', 'Religion', 'native_language', 
               'i1', 'i2', 'i3', 'Age']
    endpoint = "localhost/user/getAll"
    response = requests.get(endpoint).json()
    
    user_data = []
    
    if response:
        for data in response:
            for column in columns:
                if column not in data.keys():
                    return "missing parameter: " + str(column)
        
            user = [
                [data.get("Pays").upper(),
                 data.get("Gender").upper(),
                 data.get("Status").upper(),
                 "None",
                 data.get("Religion").upper(),
                 data.get("native_language"),
                 data.get("i1").upper(),
                 data.get("i2"),
                 data.get("i3"),
                 data.get("age")]
                ]
            user_data.append(user)
        return user_data

    
    
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8081)