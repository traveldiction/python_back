from flask import Flask, request, jsonify
from catboost import CatBoostClassifier, Pool
import json
import numpy as np
from flask import jsonify

app = Flask(__name__)


@app.route('/predict', methods=['POST'])
def predict_router():
    model_path = 'model/traveldiction.cbm'
    
    CATS = ['Pays', 'Gender', 'Status', 'Race', 'Religion', 'native_language', 
            'i1', 'i2', 'i3']
    NUMERICS = ['Age']
    
    columns = ['Pays', 'Gender', 'Status', 'Race', 'Religion', 'native_language', 
               'i1', 'i2', 'i3', 'Age']
    data = request.json
    if data:
        for column in columns:
            if column not in data.keys():
                return "missing parameter: " + str(column)
        
        model = CatBoostClassifier()
        model.load_model(model_path)
        user = [
            [data.get("Pays").upper(),
                 data.get("Gender").upper(),
                 data.get("Status").upper(),
                 "None",
                 data.get("Religion").upper(),
                 data.get("native_language"),
                 data.get("i1").upper(),
                 data.get("i2"),
                 data.get("i3"),
                 data.get("age")]
                ]
        predict_pool = Pool(data=user,
                            cat_features=CATS, 
                            feature_names=CATS+NUMERICS)
       
        preds = model.predict_proba(predict_pool)
        preds = get_three_countries(preds)
        preds = {
            "pred_1":preds[0].strip(),
            "pred_2":preds[1].strip(),
            "pred_3":preds[2].strip()
        }
        return jsonify(preds)

    else:
        return "bad request"
    

def get_three_countries(preds):
    labels = get_labels()
    preds_sorted = np.argsort(preds)
    print(preds_sorted[0])
    index = len(preds_sorted)
    first = labels[preds_sorted[0][index-1]]
    second = labels[preds_sorted[0][index-2]]
    third = labels[preds_sorted[0][index-3]]
    
    return (first, second, third)

def get_labels():
    return ['Canada', 'Russian Federation', 'Peru', 'Cyprus', 'Belarus', 'Switzerland',
 'Australia', 'Ukraine',
 'United Kingdom of Great Britain and Northern Ireland', 'New Zealand',
 'Republic of Moldova', 'Guinea', 'no_data', 'United States of America',
 'Mauritius', 'Austria', 'Timor', 'Slovakia', 'Cambodia', 'USA']
    
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8081)
    
    
